from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout
from django.shortcuts import redirect, render
from .models import Contenido


@csrf_exempt
def loggedIn(request):
    if request.user.is_authenticated:
        response = "Logged in as " + request.user.username
    else:
        response = "Not logged in. <a href='/login'>Login!</a>"
    return HttpResponse(response)


@csrf_exempt
def get_content(request, key):
    if request.method in ['POST', 'PUT']:
        if request.method == 'POST':
            valor = request.POST['valor']
        else:
            valor = request.body.decode('utf-8')
        try:
            content = Contenido.objects.get(clave=key)
            content.delete()
            content = Contenido(clave=key, valor=valor)
            content.save()
        except Contenido.DoesNotExist:
            content = Contenido(clave=key, valor=valor)
            content.save()
    context = {'authenticated': request.user.is_authenticated,
               'username': request.user.username}
    return render(request, 'cms/content.html', context)


@csrf_exempt
def index(request):
    list_content = Contenido.objects.all()
    context = {'list_content': list_content,
               'authenticated': request.user.is_authenticated}
    return render(request, 'cms/index.html', context)


@csrf_exempt
def logout_view(request):
    logout(request)
    return redirect("/cms")
